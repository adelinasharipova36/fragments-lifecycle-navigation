package com.example.fragmentslifecyclenavigation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.fragmentslifecyclenavigation.databinding.FragmentWebBinding

class WebFragment : Fragment() {

    private var binding: FragmentWebBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentWebBinding.inflate(inflater, container, false)
        return binding?.root
    }
}